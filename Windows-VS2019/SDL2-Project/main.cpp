//For exit()
#include <stdlib.h>
#include <stdio.h>
//Includes SDL Library
#include "SDL.h"

//Include SDL_Image. This library provides support for loading different types of images
#include "SDL_image.h"

	const int WINDOW_WIDTH = 600;
	const int WINDOW_HEIGHT = 800;

	const int SDL_OK = 0;
	
	const int PLAYER_SPRITE_WIDTH = 64;
	const int PLAYER_SPRITE_HEIGHT = 64;
	
	int main(int argc, char* args[])
	{
		// Declare window and renderer objects
		SDL_Window* gameWindow = nullptr;
		SDL_Renderer* gameRenderer = nullptr;

		int sdl_status = SDL_Init(SDL_INIT_EVERYTHING);

		if (sdl_status != SDL_OK) {
			//SDL did not initialise, report the error and exit
			printf("Error - SDL Inisitialisation failed! \n");
			exit(1);
		}

		gameWindow = SDL_CreateWindow("Mike's Game", //Window Title
			SDL_WINDOWPOS_UNDEFINED, // X Position
			SDL_WINDOWPOS_UNDEFINED, // Y Position
			WINDOW_HEIGHT, //height
			WINDOW_WIDTH, //width
			SDL_WINDOW_SHOWN); // Window flag



		if (gameWindow != nullptr) {
			//if the window creation suceeded creat our renderer
			gameRenderer = SDL_CreateRenderer(gameWindow, 0, 0);

			if (gameRenderer == nullptr) {
				printf("Error - SDL could not create renderer \n");
				exit(1);
			}
		}
		else
		{
			//could not create window, so do not try and create window
			printf("Error - SDL Could not create window \n");
			exit(1);
		}


		//Temporary surface used while loading image
		SDL_Surface* temp = nullptr;

		//Textures which stores the actual sprite. (this will be optimised)
		SDL_Texture* backgroundTexture = nullptr;

		//Player variables
		SDL_Texture* playerTexture = nullptr;
		//sprite size
		const int PLAYER_SPRITE_WIDTH = 64;
		const int PLAYER_SPRITE_HEIGHT = 64;
		//animation
		bool playerFlipped = false;
		SDL_RendererFlip flip;

		//Source and target rects
		SDL_Rect standRight; //standing sprite
		SDL_Rect targetRectangle;

		float playerSpeed = 50.0f;
		float playerX = 250.0f;
		float playerY = 250.0f;

		//Window control
		SDL_Event event;
		bool quit = false;
		float horizontalInput = 0.0f;
		float verticalInput = 0.0f;

		//Timing variables
		unsigned int currentTimeIndex;
		unsigned int prevTimeIndex;
		unsigned int timeDelta;
		float timeDeltaInSeconds;

		/*************************
		* Setup background image *
		* ************************/

		//Load sprite to temp
		temp = IMG_Load("assets/images/background.bmp");

		if (temp == nullptr) {
			printf("Error - could not load background");
		}

		//Create a texture object from the loaded image. We need the renderer we are going to use to draw this as well.
		//This provides information about the target format to aid optimisation
		backgroundTexture = SDL_CreateTextureFromSurface(gameRenderer, temp);

		//Clean up. We are done with the temporary image now our textures has been created
		SDL_FreeSurface(temp);
		temp = nullptr;

		/**************************
		* Setup player texture *
		***************************/

		//Load the sprite to our temp surface
		temp = IMG_Load("assets/images/walker1.png");

		//Create a texture object from the loaded image
		playerTexture = SDL_CreateTextureFromSurface(gameRenderer, temp);

		//clean up - we are done with temp again
		SDL_FreeSurface(temp);
		temp = nullptr;

		//Setup source and destination rectangles
		targetRectangle.x = 250;
		targetRectangle.y = 250;
		targetRectangle.w = PLAYER_SPRITE_WIDTH;
		targetRectangle.h = PLAYER_SPRITE_HEIGHT;

		standRight.x = 2 * PLAYER_SPRITE_WIDTH;
		standRight.y = 2 * PLAYER_SPRITE_HEIGHT;
		standRight.w = PLAYER_SPRITE_WIDTH;
		standRight.h = PLAYER_SPRITE_HEIGHT;

		prevTimeIndex = SDL_GetTicks();


		//Event loop
		while (!quit) 
		{ //While quit is not true
			//Calculate time elapsed
			
			currentTimeIndex = SDL_GetTicks(); // time in milliseconds
			timeDelta = currentTimeIndex - prevTimeIndex;
			timeDeltaInSeconds = timeDelta * 0.001;

			//stores current time index to prevTimeIndex for next frame
			prevTimeIndex = currentTimeIndex;

			//Handle input
			if (SDL_PollEvent(&event)) 
			{ //poll for events
				switch (event.type) 
				{
				case SDL_QUIT: //if the user has closed out the window

						quit = true; //quit the program

					break;		

					//Key pressed event
				case SDL_KEYDOWN:
					switch (event.key.keysym.sym) 
					{
					case SDLK_ESCAPE:
						quit = true;
						break;

					case SDLK_UP:
						verticalInput = -1.0f; // move up
						break;

					case SDLK_DOWN:
						verticalInput = 1.0f;
						break;

					case SDLK_RIGHT:
						horizontalInput = 1.0f;
						break;

					case SDLK_LEFT:
						horizontalInput = -1.0f;
						break;
					}
				break;

				case SDL_KEYUP:
					switch (event.key.keysym.sym) {
					case SDLK_ESCAPE:
						//nothing to do here
						break;

					case SDLK_UP: //fall through
					case SDLK_DOWN:
						verticalInput = 0.0f;
						break;

					case SDLK_RIGHT:
					case SDLK_LEFT:
						horizontalInput = 0.0f;
						break;
					}
					break;
				default:

						//not an errror. ignoring events

					break;
				}
			}

			//Process player input

			//Calculate player velocity
			//Note : no account taken for diagonal
			float xVelocity = verticalInput * playerSpeed;
			float yVelocity = horizontalInput * playerSpeed;

			//Update player

			//Calculate the distance travelled since last update
			float yMovement = timeDeltaInSeconds * xVelocity;
			float xMovement = timeDeltaInSeconds * yVelocity;

			//Update player position
			playerX += xMovement;
			playerY += yMovement;

			//Move sprite to nearest pixel location
			targetRectangle.y = round(playerY);
			targetRectangle.x = round(playerX);
		
			//Render. Draw stuff

			//1. Clear the screen
			SDL_SetRenderDrawColor(gameRenderer, 0, 0, 0, 255);
			//Color provided as red, green, blue, alpha
			//(transparancy) values (i.e. RGBA)

			SDL_RenderClear(gameRenderer);

			//2. Draw the image
			SDL_RenderCopy(gameRenderer, backgroundTexture, NULL, NULL);

			SDL_RenderCopy(gameRenderer,
				playerTexture,
				&standRight,
				&targetRectangle);

			//3. Present the current frame to the screen
			SDL_RenderPresent(gameRenderer);

			//Pause to allow the image to be seen
			//SDL_Delay(5000);

		} //end of while


	//Clean up	
	//SDL_RenderClear(gameRenderer);

	SDL_DestroyTexture(backgroundTexture);
	backgroundTexture = nullptr;

	SDL_DestroyTexture(playerTexture);
	playerTexture = nullptr;

	SDL_DestroyRenderer(gameRenderer);
	gameRenderer = nullptr;

	SDL_DestroyWindow(gameWindow);
	gameWindow = nullptr;

	//Shut down SDL, clear resources etc
	SDL_Quit();

	//Exit the program
	exit(0);
}